USE master

GO
IF EXISTS(select * from sys.databases where name='dbCampeonatoPlus')
DROP DATABASE dbCampeonatoPlus
GO

CREATE DATABASE dbCampeonatoPlus;
GO
USE  dbCampeonatoPlus;
GO


CREATE TABLE Administrador(
	idAdmin INT IDENTITY(1,1),
	usuario VARCHAR(255) NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	pass VARCHAR(255) NOT NULL,
	PRIMARY KEY(idAdmin)
);


CREATE TABLE Torneo(
	idTorneo INT IDENTITY(1,1),
	nombre VARCHAR(255),
	fechaIncio VARCHAR(255),
	fechaFin VARCHAR(255),
	PRIMARY KEY(idTorneo)
);


CREATE TABLE Equipo(
	idEquipo INT IDENTITY(1,1),
	nombre VARCHAR(255),
	puntos INT ,
	partidosJugados INT,
	partidosGanados INT,
	partidosPerdidos INT,
	partidosEmpatados INT,
	golesAFavor INT,
	goleEnContra INT,
	PRIMARY KEY(idEquipo)
);

CREATE TABLE Jugador(
	idJugador INT IDENTITY(1,1),
	nombre VARCHAR(255),
	idEquipo INT,
	cantidadDeGoles INT,
	PRIMARY KEY (idJugador),
	FOREIGN KEY (idEquipo) REFERENCES Equipo(idEquipo)
);

CREATE TABLE ResultadoMesa(
	idResultadoMesa	INT IDENTITY(1,1),
	golesA INT,
	golesB INT,
	descripcion VARCHAR(255),
	idEquipoGanador INT,
	PRIMARY KEY(idResultadoMesa),
	FOREIGN KEY(idEquipoGanador) REFERENCES Equipo(idEquipo)
);

CREATE TABLE ResultadoJuego(
	idResultadoJuego INT IDENTITY(1,1),
	golesA INT,
	golesB INT,
	PRIMARY KEY(idResultadoJuego)
);

CREATE TABLE Juego(
	idJuego INT IDENTITY(1,1),
	pendiente BIT NOT NULL,
	idEquipoA INT NOT NULL,
	idEquipoB INT NOT NULL,
	hora VARCHAR(5),
	fecha VARCHAR(10) NOT NULL,
	idTorneo INT NOT NULL,
	idResultadoMesa INT,
	idResultadoJuego INT ,
	PRIMARY KEY (idJuego),
	FOREIGN KEY (idEquipoA) REFERENCES Equipo(idEquipo),
	FOREIGN KEY (idEquipoB) REFERENCES Equipo(idEquipo),
	FOREIGN KEY (idTorneo) REFERENCES Torneo(idTorneo),
	FOREIGN KEY (idResultadoMesa) REFERENCES ResultadoMesa(idResultadoMesa),
	FOREIGN KEY (idResultadoJuego) REFERENCES ResultadoJuego(idResultadoJuego)
);



GO
CREATE TRIGGER CrearResultados
    ON Juego
    FOR INSERT
    AS
    BEGIN
		DECLARE @idJuego INT = @@IDENTITY;
		INSERT INTO ResultadoJuego VALUES (0,0);
		DECLARE @idResultadoJuego INT=@@IDENTITY
		
		INSERT INTO ResultadoMesa (golesA,golesB) VALUES (0,0);
		DECLARE @idResultadoMesa INT=@@IDENTITY;
		UPDATE Juego SET idResultadoJuego =@idResultadoJuego ,idResultadoMesa=@idResultadoMesa WHERE Juego.idJuego=@idJuego;
    END

CREATE TRIGGER ActualizarMesa
    ON ResultadoJuego
    FOR UPDATE
    AS
    BEGIN
    DECLARE @idJuego INT = (Select Juego.idJuego From Juego where Juego.idResultadoJuego= @@IDENTITY);
    DECLARE @golesA INT = (Select ResultadoJuego.golesA From ResultadoJuego where ResultadoJuego.idResultadoJuego=@@IDENTITY);
    DECLARE @golesB INT = (Select ResultadoJuego.golesB From ResultadoJuego where ResultadoJuego.idResultadoJuego=@@IDENTITY);

    UPDATE ResultadoMesa SET golesA=@golesA, golesB=@golesB Where ResultadoMesa.idResultadoMesa=Juego.idResultadoJuego; 
    DECLARE @idResultadoJuego INT=@@IDENTITY
    
    INSERT INTO ResultadoMesa (golesA,golesB) VALUES (0,0);
    DECLARE @idResultadoMesa INT=@@IDENTITY;
    UPDATE Juego SET idResultadoJuego =@idResultadoJuego ,idResultadoMesa=@idResultadoMesa WHERE Juego.idJuego=@idJuego;
    END

  


GO
CREATE PROCEDURE ActualizarResultadoJuego
    @golesA int = 0,
    @golesB int = 0,
	@idResultadoJuego int = 0
AS
BEGIN
  	UPDATE ResultadoJuego SET golesA = @golesA ,golesB = @golesB WHERE ResultadoJuego.idResultadoJuego= @idResultadoJuego;
  	UPDATE Juego SET pendiente= 1 where Juego.idResultadoJuego=@idResultadoJuego;   
 
END
GO

--insertando Administradores

INSERT INTO [dbo].[Administrador]
           ([usuario]
           ,[nombre]
           ,[pass])
     VALUES
           ('Usuario'
           ,'Pablo',
           '123')


INSERT INTO [dbo].[Administrador]
           ([usuario]
           ,[nombre]
           ,[pass])
     VALUES
           ('SA'
           ,'ADMINISTADOR'
           ,'123')

INSERT INTO [dbo].[Administrador]
           ([usuario]
           ,[nombre]
           ,[pass])
     VALUES
           ('admin'
           ,'admin'
           ,'123')



--insertando Torneo
INSERT INTO [dbo].[Torneo]
           ([nombre]
           ,[fechaIncio]
           ,[fechaFin])
     VALUES
           ('Campeonato 2015'
           ,'1/1/2015'
           ,'1/2/2015')


--equipos
INSERT INTO Equipo VALUES ('Equipo 1', 0,0,0,0,0,0,0);
INSERT INTO Equipo VALUES ('Equipo 2', 0,0,0,0,0,0,0);
INSERT INTO Equipo VALUES ('Equipo 3', 0,0,0,0,0,0,0);
INSERT INTO Equipo VALUES ('Equipo 4', 0,0,0,0,0,0,0);
INSERT INTO Equipo VALUES ('Equipo 5', 0,0,0,0,0,0,0);
INSERT INTO Equipo VALUES ('Equipo 6', 0,0,0,0,0,0,0);
INSERT INTO Equipo VALUES ('Equipo 7', 0,0,0,0,0,0,0);

--

INSERT INTO Jugador VALUES('Jugador 1',1,1);
INSERT INTO Jugador VALUES('Jugador 2',1,1);
INSERT INTO Jugador VALUES('Jugador 3',1,1);
INSERT INTO Jugador VALUES('Jugador 4',1,1);
INSERT INTO Jugador VALUES('Jugador 5',1,1);
INSERT INTO Jugador VALUES('Jugador 6',2,1);
INSERT INTO Jugador VALUES('Jugador 7',2,1);
INSERT INTO Jugador VALUES('Jugador 8',2,1);
INSERT INTO Jugador VALUES('Jugador 9',2,1);
INSERT INTO Jugador VALUES('Jugador 10',2,1);
INSERT INTO Jugador VALUES('Jugador 11',3,1);
INSERT INTO Jugador VALUES('Jugador 12',3,1);
INSERT INTO Jugador VALUES('Jugador 13',3,1);
INSERT INTO Jugador VALUES('Jugador 14',3,1);
INSERT INTO Jugador VALUES('Jugador 15',3,1);
INSERT INTO Jugador VALUES('Jugador 16',4,1);
INSERT INTO Jugador VALUES('Jugador 17',4,1);
INSERT INTO Jugador VALUES('Jugador 18',4,1);
INSERT INTO Jugador VALUES('Jugador 19',4,1);
INSERT INTO Jugador VALUES('Jugador 20',4,1);
INSERT INTO Jugador VALUES('Jugador 21',5,1);
INSERT INTO Jugador VALUES('Jugador 22',5,1);
INSERT INTO Jugador VALUES('Jugador 23',5,1);
INSERT INTO Jugador VALUES('Jugador 24',5,1);
INSERT INTO Jugador VALUES('Jugador 25',5,1);
--
INSERT INTO [dbo].[Juego]
           ([pendiente]
           ,[idEquipoA]
           ,[idEquipoB]
           ,[hora]
           ,[fecha]
           ,[idTorneo]
      )
     VALUES
           (0
           ,1
           ,2
           ,'8:10'
           ,'2/1/15'
           ,1
         )
     INSERT INTO [dbo].[Juego]
           ([pendiente]
           ,[idEquipoA]
           ,[idEquipoB]
           ,[hora]
           ,[fecha]
           ,[idTorneo]
      )
     VALUES
           (0
           ,1
           ,3
           ,'8:10'
           ,'2/1/15'
           ,1
         )

		 INSERT INTO [dbo].[Juego]
           ([pendiente]
           ,[idEquipoA]
           ,[idEquipoB]
           ,[hora]
           ,[fecha]
           ,[idTorneo]
      )
     VALUES
           (0
           ,1
           ,4
           ,'8:10'
           ,'2/1/15'
           ,1
         )
		 INSERT INTO [dbo].[Juego]
           ([pendiente]
           ,[idEquipoA]
           ,[idEquipoB]
           ,[hora]
           ,[fecha]
           ,[idTorneo]
      )
     VALUES
           (0
           ,1
           ,5
           ,'8:10'
           ,'2/1/15'
           ,1
         )

INSERT INTO [dbo].[Juego]
           ([pendiente]
           ,[idEquipoA]
           ,[idEquipoB]
           ,[hora]
           ,[fecha]
           ,[idTorneo]
      )
     VALUES
           (0
           ,2
           ,2
           ,'8:10'
           ,'2/1/15'
           ,1
         );
     INSERT INTO [dbo].[Juego]
           ([pendiente]
           ,[idEquipoA]
           ,[idEquipoB]
           ,[hora]
           ,[fecha]
           ,[idTorneo]
      )
     VALUES
           (0
           ,2
           ,3
           ,'8:10'
           ,'2/1/15'
           ,1
         )

		 INSERT INTO [dbo].[Juego]
           ([pendiente]
           ,[idEquipoA]
           ,[idEquipoB]
           ,[hora]
           ,[fecha]
           ,[idTorneo]
      )
     VALUES
           (0
           ,2
           ,4
           ,'8:10'
           ,'2/1/15'
           ,1
         )
		 INSERT INTO [dbo].[Juego]
           ([pendiente]
           ,[idEquipoA]
           ,[idEquipoB]
           ,[hora]
           ,[fecha]
           ,[idTorneo]
      )
     VALUES
           (0
           ,2
           ,5
           ,'8:10'
           ,'2/1/15'
           ,1
         )

GO

CREATE VIEW ObjetoJUego AS 
	SELECT Juego.idJuego, Juego.pendiente,
		(SELECT Equipo.nombre From Equipo where idEquipo= Juego.idEquipoA) EquipoA,  
		(SELECT Equipo.nombre From Equipo where idEquipo= Juego.idEquipoB) EquipoB, Juego.hora, Juego.fecha  from Juego

CREATE VIEW ObjetoJuegoPendiente AS 
	SELECT Juego.idJuego numero, 
		(SELECT Equipo.nombre From Equipo where idEquipo= Juego.idEquipoA) EquipoA,  
		(SELECT Equipo.nombre From Equipo where idEquipo= Juego.idEquipoB) EquipoB, Juego.hora, Juego.fecha  from Juego where Juego.pendiente=0



CREATE VIEW ObjetoJuegoPasado AS 
	SELECT Juego.idJuego, 
		(SELECT Equipo.nombre From Equipo where idEquipo= Juego.idEquipoA) EquipoA, 
		(SELECT ResultadoJuego.golesA From ResultadoJuego where ResultadoJuego.idResultadoJuego=Juego.idResultadoJuego) golesA , 
		(SELECT Equipo.nombre From Equipo where idEquipo= Juego.idEquipoB) EquipoB,
		(SELECT ResultadoJuego.golesB From ResultadoJuego where ResultadoJuego.idResultadoJuego=Juego.idResultadoJuego) golesB, 
		Juego.fecha  from Juego where Juego.pendiente=1;

CREATE VIEW PosicionesDeEquipos AS 
  SELECT TOP 100 * FROM Equipo ORDER BY Equipo.puntos  DESC;
  

GO
Exec ActualizarResultadoJuego 1,1 2 ;
GO

