﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Campeonato.Models;

namespace Campeonato.Controllers
{
    public class PosicionesDeEquiposController : ApiController
    {
        private dbCampeonatoPlusEntities db = new dbCampeonatoPlusEntities();

        // GET api/PosicionesDeEquipos
        public IQueryable<PosicionesDeEquipos> GetPosicionesDeEquipos()
        {
            return db.PosicionesDeEquipos;
        }

        // GET api/PosicionesDeEquipos/5
        [ResponseType(typeof(PosicionesDeEquipos))]
        public IHttpActionResult GetPosicionesDeEquipos(int id)
        {
            PosicionesDeEquipos posicionesdeequipos = db.PosicionesDeEquipos.Find(id);
            if (posicionesdeequipos == null)
            {
                return NotFound();
            }

            return Ok(posicionesdeequipos);
        }

        // PUT api/PosicionesDeEquipos/5
        public IHttpActionResult PutPosicionesDeEquipos(int id, PosicionesDeEquipos posicionesdeequipos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != posicionesdeequipos.idEquipo)
            {
                return BadRequest();
            }

            db.Entry(posicionesdeequipos).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PosicionesDeEquiposExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/PosicionesDeEquipos
        [ResponseType(typeof(PosicionesDeEquipos))]
        public IHttpActionResult PostPosicionesDeEquipos(PosicionesDeEquipos posicionesdeequipos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PosicionesDeEquipos.Add(posicionesdeequipos);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = posicionesdeequipos.idEquipo }, posicionesdeequipos);
        }

        // DELETE api/PosicionesDeEquipos/5
        [ResponseType(typeof(PosicionesDeEquipos))]
        public IHttpActionResult DeletePosicionesDeEquipos(int id)
        {
            PosicionesDeEquipos posicionesdeequipos = db.PosicionesDeEquipos.Find(id);
            if (posicionesdeequipos == null)
            {
                return NotFound();
            }

            db.PosicionesDeEquipos.Remove(posicionesdeequipos);
            db.SaveChanges();

            return Ok(posicionesdeequipos);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PosicionesDeEquiposExists(int id)
        {
            return db.PosicionesDeEquipos.Count(e => e.idEquipo == id) > 0;
        }
    }
}