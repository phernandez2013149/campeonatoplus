﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Campeonato.Models;

namespace Campeonato.Controllers
{
    public class JuegosPasadosController : ApiController
    {
        private dbCampeonatoPlusEntities db = new dbCampeonatoPlusEntities();

        // GET api/JuegosPasados
        public IQueryable<ObjetoJuegoPasado> GetObjetoJuegoPasado()
        {
            return db.ObjetoJuegoPasado;
        }

        // GET api/JuegosPasados/5
        [ResponseType(typeof(ObjetoJuegoPasado))]
        public IHttpActionResult GetObjetoJuegoPasado(int id)
        {
            ObjetoJuegoPasado objetojuegopasado = db.ObjetoJuegoPasado.Find(id);
            if (objetojuegopasado == null)
            {
                return NotFound();
            }

            return Ok(objetojuegopasado);
        }

        // PUT api/JuegosPasados/5
        public IHttpActionResult PutObjetoJuegoPasado(int id, ObjetoJuegoPasado objetojuegopasado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != objetojuegopasado.idJuego)
            {
                return BadRequest();
            }

            db.Entry(objetojuegopasado).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ObjetoJuegoPasadoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/JuegosPasados
        [ResponseType(typeof(ObjetoJuegoPasado))]
        public IHttpActionResult PostObjetoJuegoPasado(ObjetoJuegoPasado objetojuegopasado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ObjetoJuegoPasado.Add(objetojuegopasado);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = objetojuegopasado.idJuego }, objetojuegopasado);
        }

        // DELETE api/JuegosPasados/5
        [ResponseType(typeof(ObjetoJuegoPasado))]
        public IHttpActionResult DeleteObjetoJuegoPasado(int id)
        {
            ObjetoJuegoPasado objetojuegopasado = db.ObjetoJuegoPasado.Find(id);
            if (objetojuegopasado == null)
            {
                return NotFound();
            }

            db.ObjetoJuegoPasado.Remove(objetojuegopasado);
            db.SaveChanges();

            return Ok(objetojuegopasado);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ObjetoJuegoPasadoExists(int id)
        {
            return db.ObjetoJuegoPasado.Count(e => e.idJuego == id) > 0;
        }
    }
}