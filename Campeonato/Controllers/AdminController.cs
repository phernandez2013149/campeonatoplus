﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Campeonato.Models;

namespace Campeonato.Controllers
{
    public class AdminController : ApiController
    {
        private dbCampeonatoPlusEntities db = new dbCampeonatoPlusEntities();

        // GET api/Admin
        public IQueryable<Administrador> GetAdministrador()
        {
            return db.Administrador;
        }

        // GET api/Admin/5
        [ResponseType(typeof(Administrador))]
        public IHttpActionResult GetAdministrador(int id)
        {
            Administrador administrador = db.Administrador.Find(id);
            if (administrador == null)
            {
                return NotFound();
            }

            return Ok(administrador);
        }

        // PUT api/Admin/5
        public IHttpActionResult PutAdministrador(int id, Administrador administrador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != administrador.idAdmin)
            {
                return BadRequest();
            }

            db.Entry(administrador).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdministradorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Admin
        [ResponseType(typeof(Administrador))]
        public IHttpActionResult PostAdministrador(Administrador administrador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Administrador.Add(administrador);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = administrador.idAdmin }, administrador);
        }

        // DELETE api/Admin/5
        [ResponseType(typeof(Administrador))]
        public IHttpActionResult DeleteAdministrador(int id)
        {
            Administrador administrador = db.Administrador.Find(id);
            if (administrador == null)
            {
                return NotFound();
            }

            db.Administrador.Remove(administrador);
            db.SaveChanges();

            return Ok(administrador);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AdministradorExists(int id)
        {
            return db.Administrador.Count(e => e.idAdmin == id) > 0;
        }
    }
}