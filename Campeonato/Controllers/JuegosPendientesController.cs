﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Campeonato.Models;

namespace Campeonato.Controllers
{
    public class JuegosPendientesController : ApiController
    {
        private dbCampeonatoPlusEntities db = new dbCampeonatoPlusEntities();

        // GET api/JuegosPendientes
        public IQueryable<ObjetoJuegoPendiente> GetObjetoJuegoPendiente()
        {
            return db.ObjetoJuegoPendiente;
        }

        // GET api/JuegosPendientes/5
        [ResponseType(typeof(ObjetoJuegoPendiente))]
        public IHttpActionResult GetObjetoJuegoPendiente(int id)
        {
            ObjetoJuegoPendiente objetojuegopendiente = db.ObjetoJuegoPendiente.Find(id);
            if (objetojuegopendiente == null)
            {
                return NotFound();
            }

            return Ok(objetojuegopendiente);
        }

        // PUT api/JuegosPendientes/5
        public IHttpActionResult PutObjetoJuegoPendiente(int id, ObjetoJuegoPendiente objetojuegopendiente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != objetojuegopendiente.numero)
            {
                return BadRequest();
            }

            db.Entry(objetojuegopendiente).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ObjetoJuegoPendienteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/JuegosPendientes
        [ResponseType(typeof(ObjetoJuegoPendiente))]
        public IHttpActionResult PostObjetoJuegoPendiente(ObjetoJuegoPendiente objetojuegopendiente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ObjetoJuegoPendiente.Add(objetojuegopendiente);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = objetojuegopendiente.numero }, objetojuegopendiente);
        }

        // DELETE api/JuegosPendientes/5
        [ResponseType(typeof(ObjetoJuegoPendiente))]
        public IHttpActionResult DeleteObjetoJuegoPendiente(int id)
        {
            ObjetoJuegoPendiente objetojuegopendiente = db.ObjetoJuegoPendiente.Find(id);
            if (objetojuegopendiente == null)
            {
                return NotFound();
            }

            db.ObjetoJuegoPendiente.Remove(objetojuegopendiente);
            db.SaveChanges();

            return Ok(objetojuegopendiente);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ObjetoJuegoPendienteExists(int id)
        {
            return db.ObjetoJuegoPendiente.Count(e => e.numero == id) > 0;
        }
    }
}