﻿var ViewIndex = function () {
    var self = this;

    //CREACION DE OBJETOS OBSERVABLES
    self.listaDeJuegosPendientes = ko.observableArray(); 
    self.listaDeJuegosPasados = ko.observableArray();
    self.listaDeEquiposPorPosicion = ko.observableArray();

    self.error = ko.observable();
    self.detail = ko.observable();
    self.editar = ko.observable();

    //DEFINIR LAS RUTAS DEL API
    var juegosPasadosUri = '/api/JuegosPasados/';
    var juegosPendientesUri = '/api/JuegosPendientes/';
    var posicionEquiposUri = '/api/PosicionesDeEquipos/';


   function ajaxHelper(uri, method, data) {
        self.error(''); 
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? alert(JSON.stringify(data)) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

 
    self.getContactoDetail = function (item) {
        ajaxHelper(juegosPendientesUri + item.ID, 'GET').done(function (data) {
            self.detail(data);
        });
    }
    
 //ACTUALIZAR LISTA DE JUEGOS PENDIENTES
    function getJuegosPendientes() {
        ajaxHelper(juegosPendientesUri, 'GET').done(function (data) {
            self.listaDeJuegosPendientes(data);
        });
    }
    


  //ACTUALIZAR LA LISTA DE JUEGOS PASADOS
    function getJuegosPasados() {
        ajaxHelper(juegosPasadosUri, 'GET').done(function (data) {
            self.listaDeJuegosPasados(data);
        });
    }

    function getEquiposPorPosicion() {
        ajaxHelper(posicionEquiposUri, 'GET').done(function (data) {
            self.listaDeEquiposPorPosicion(data);
        });
    }
    //CARGAR A MEMORIA
    getJuegosPasados();
    getJuegosPendientes();
    getEquiposPorPosicion();
   
};

//REALIZA EL BINDING DE LA VISTA CON JS
ko.applyBindings(new ViewIndex());